const fastify = require ('fastify') ({ logger : true})


fastify.register(require('./routes/user'))


const start = async () => {
    try {
      await fastify.listen(3000)
      fastify.log.info('server running on port 3000')
    } catch (error) {
      fastify.log.error(error)
      process.exit(1)
    }
  }
  
  start()