const {Register, LoginUser, GetUser} = require ('../controller/user.controller')

async function routes (fastify, options) {

  fastify.post('/SignUp', Register)
  fastify.post('/Login', LoginUser)
  fastify.get('/:id', GetUser)

}
module.exports = routes


